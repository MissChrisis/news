//
//  NewTableViewCell.swift
//  News
//
//  Created by christina on 5/25/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import UIKit

class ResourceTableViewCell: UITableViewCell {

	@IBOutlet weak var lableResouce: UIImageView!
	
	@IBOutlet weak var nameResource: UIButton!
	
	@IBAction func lableButton(_ sender: UIButton) {
		
		switch sender.titleLabel?.text {
		case "Yandex":
			RSSParser.shared.getXMLDataFromServer(url: "https://news.yandex.ru/index.rss")
		case "tut.by":
			RSSParser.shared.getXMLDataFromServer(url: "https://news.tut.by/rss/all.rss")
		default:
			return
		}
	}
	
	func update(resource: Resource)  {
		
		nameResource.setTitle(resource.nameResource, for: .normal) 
		lableResouce.image = UIImage(named: resource.image)
	
	}

}
