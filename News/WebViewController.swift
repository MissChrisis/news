//
//  WebViewController.swift
//  News
//
//  Created by christina on 5/26/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
	
	var linkOfNew: String?

	@IBOutlet weak var webView: UIWebView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		if linkOfNew != nil {
			newDidChange(url: linkOfNew!)
		} else {
			return
		}
    }
	
	func newDidChange(url: String) {
		
		guard let webURL = URL(string: url) else {
			return
		}
		let webRequest = URLRequest(url: webURL)
		
		webView?.loadRequest(webRequest)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	
}
