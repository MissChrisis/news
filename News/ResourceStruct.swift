//
//  ResourceStruct.swift
//  News
//
//  Created by christina on 5/25/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import Foundation

struct Resource {
	
	var image: String
	var nameResource: String
}
