//
//  ViewController.swift
//  News
//
//  Created by christina on 5/24/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
	
	var newsArray = [New]()
	
	@IBOutlet weak var table: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		RSSParser.shared.delegate = self
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "New",
			let vc = segue.destination as? WebViewController,
			let link = sender as? String {
			vc.linkOfNew = link
		}
	}
}

extension NewsViewController: XMLParserResultDelegat {
	
	func didSuccessGetNews(news: [New]) {
		DispatchQueue.main.async {
			self.newsArray = news
			self.table.reloadData()
		}
	}
}

extension NewsViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return newsArray.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellNew", for: indexPath) as? NewsTableViewCell else {
			fatalError("Error")
		}
		let data = newsArray[indexPath.row]
		cell.buttonNew.setTitle(data.title, for: .normal)
		cell.delegate = self
		return cell
	}
	
}

extension NewsViewController: NewDidChangeDelegate {
	
	func newDidChange(cell: NewsTableViewCell) {
		if let indexPath = self.table.indexPath(for: cell) {
			self.performSegue(withIdentifier: "New", sender: self.newsArray[indexPath.row].link)
		}
	}
	
	
}
