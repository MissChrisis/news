//
//  RSSParser.swift
//  News
//
//  Created by christina on 5/24/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import Foundation

protocol XMLParserResultDelegat: class {

	func didSuccessGetNews(news: [New])

}

class RSSParser: NSObject {
	
	static var shared = RSSParser()
	
	private override init() {}
	
	var newTitle = String()
	var newLink = String()
	var newDescription = String()
	var newElement = String()
	var newsArray: [New] = []
	var delegate: XMLParserResultDelegat?
	
	
	func getXMLDataFromServer(url: String) {
		
		guard let url = URL(string: url) else {
			return
		}
		
		//Creating data task
		let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
			
			guard let data = data else {
				print("dataTaskWithRequest error: \(String(describing: error))")
				return
			}
			
			self.newsArray = [New]()
			let parser = XMLParser(data: data)
			print(data)
			parser.delegate = self
			parser.parse()
			
		}
		task.resume()
	}
}

extension RSSParser: XMLParserDelegate {
	
	//MARK:- XML Delegate methods
	func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
		
		if elementName == "item" {
			newTitle = String()
			newLink = String()
			newDescription = String()
		}
		
		self.newElement = elementName
	}
	
	func parser(_ parser: XMLParser, foundCharacters string: String) {
		
		let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		
		if (!data.isEmpty) {
			
			if newElement == "title" {
				newTitle = data
			} else if newElement == "link" {
				newLink += data
			}
		}
	}
	
	func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
		
		if elementName == "item" {
			
			let new = New(title: newTitle, link: newLink)
			newsArray.append(new)
			print(new)
		}
	}
	
	func parserDidEndDocument(_ parser: XMLParser) {
		
		self.delegate?.didSuccessGetNews(news: self.newsArray)
	}
	
	func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
		print("parseErrorOccurred: \(parseError)")
	}
}
