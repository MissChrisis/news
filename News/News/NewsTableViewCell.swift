//
//  NewsTableViewCell.swift
//  News
//
//  Created by christina on 5/25/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import UIKit

protocol NewDidChangeDelegate: class {
	func newDidChange(cell: NewsTableViewCell)
}

class NewsTableViewCell: UITableViewCell {
	
	weak var delegate: NewDidChangeDelegate!
	var link = String()
	
	@IBOutlet weak var buttonNew: UIButton!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		buttonNew.titleLabel?.numberOfLines = 0
	}

	@IBAction func getNewAction(_ sender: Any) {
		
		delegate?.newDidChange(cell: self)

	}
	
}
