//
//  ViewController.swift
//  News
//
//  Created by christina on 5/25/19.
//  Copyright © 2019 kvolchek. All rights reserved.
//

import UIKit

class ResourceViewController: UIViewController {
	
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var tableResource: UITableView!
	
	var searchResource = [String]()
	var resourceList: [Resource] = [Resource(image: "Yandex_logo_en.svg", nameResource: "Yandex"), Resource(image: "tut.by", nameResource: "tut.by")]
	
	lazy var searchQuene = OperationQueue()
	
	var isSearchProgress: Bool = false {
		didSet {
			tableResource.reloadData()
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		
		searchBar.delegate = self
		searchQuene.maxConcurrentOperationCount = 1

    }
}

extension ResourceViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		return isSearchProgress ? searchResource.count : resourceList.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ResourceTableViewCell else {
			fatalError("Error")
		}
		cell.update(resource: resourceList[indexPath.row])
		
		if isSearchProgress {
			cell.nameResource.setTitle(searchResource[indexPath.row], for: .normal)
			for r in resourceList {
				if r.nameResource == searchResource[indexPath.row] {
					cell.lableResouce.image = UIImage(named: r.image)
					
				}
			}
		} else {
			cell.nameResource.setTitle(resourceList[indexPath.row].nameResource, for: .normal)
		}
		return cell
	}
}

extension ResourceViewController: UISearchBarDelegate {
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		
		var newsArray = [String]()
		for item in resourceList {
			let i = item.nameResource
			newsArray.append(i)
		}
		searchResource = newsArray.filter({$0.prefix(searchText.count) == searchText})
		isSearchProgress = true
		tableResource.reloadData()
	}
	
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		isSearchProgress = false
		searchBar.resignFirstResponder()
		tableResource.reloadData()
	}

}
